package com.example.usersapi.repositories;

import com.example.usersapi.models.Event;
import org.springframework.data.repository.CrudRepository;
public interface EventRepository extends CrudRepository<Event, Long> {
}
