package com.example.usersapi.models;


import lombok.*;
import javax.persistence.*;

@Data
@AllArgsConstructor @NoArgsConstructor @Getter @Setter
@Entity @Table (name="EVENTS")
public class Event {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "EVENT_NAME")
    private String eventName;

    @Column(name = "CITY")
    private String city;

    @Column(name = "COUNTRY")
    private String country;

}
