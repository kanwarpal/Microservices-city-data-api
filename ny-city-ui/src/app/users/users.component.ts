import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import { HttpModule } from '@angular/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  private users: any;
  private object_user = {
  "userName": '',
  "firstName": '',
  "lastName": ''
 };
  constructor(private http: Http) { }
  ngOnInit() {
    this.http.get('/users/users')
    .subscribe(response => this.users = response.json());
  }
  deleteUser(id) {
    return this.http.delete(`/users/users/${id}`).subscribe(response=>this.users=null);
    
  }
  addUser() {
    console.log('Adding:'+this.object_user.firstName);
    return this.http.post('/users/users', this.object_user).subscribe(respose=>{
      this.users.push(respose.json());
    this.object_user.userName='';
      this.object_user.firstName='';
      this.object_user.lastName='';
  })
}


}
