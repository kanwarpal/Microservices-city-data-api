import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
data: any;
  constructor(
    private http: Http
  ) { }

  ngOnInit() {
  }
  getData(){
    this.http.get('https://data.cityofnewyork.us/resource/buex-bi6w.json')
    .toPromise()
    .then(response => this.data = response.json());
  }
  

}
